#This i s the main for Palantir a GUI for Sauron


import sys
from PyQt5.QtWidgets import QApplication
from Coordinators import MainCoordinator

def main():
    """Main function."""
    # Create an instance of QApplication
    sauronGui = QApplication(sys.argv)
    MainCoordinator.MainCoordinator()

    # Execute the calculator's main loop
    sys.exit(sauronGui.exec_())


if __name__ == '__main__':
    main()
