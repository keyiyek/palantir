#ViewPageLocationsController

import datetime

from Coordinators.SchedulerController import SchedulerController
from Coordinators.WidgetControllers.ListItemSchedulerController import ListItemSchedulerController
from Views.ViewPageLocations import ViewPageLocations


class ViewPageLocationsController:

    def __init__(self, coordinator, page, schedulerConf, autoPublish, templateName, secondaryList = None):
        self._coordinator = coordinator
        self.page = page
        print(self.page)
        self.schedulerConf = schedulerConf
        self.autoPublish = autoPublish
        self.templateName = templateName
        self.secondaryList = secondaryList
        self.locationsList = []

        self._status = ListItemSchedulerController(self)
        self._view = ViewPageLocations(self)

        self._loadView()
        #self._startScheduler()

    #region INTERNAL SETUP
    def _loadView(self):
        self._view.show()

    def startScheduler(self):
        if self.schedulerConf[0]:
            self.scheduler = SchedulerController(self, self.schedulerConf[1], self.schedulerConf[2])
        else:
            self.scrape()

    def _nextRuns(self):
        run = self.currentTime
        if self.schedulerConf[0]:
            next = (datetime.timedelta(minutes=self.schedulerConf[1]) + self.now).strftime("%H:%M:%S")
            delta = str(self.schedulerConf[1])
        else:
            next = ""
            delta = ""
        return [run, next, delta]
    #endregion

    #region OUTGOING DOWNWARD
    def scrape(self):
        print("Yes, Master! Let's go hunting again.")
        self.now = datetime.datetime.now()
        self.currentTime = self.now.strftime("%H:%M:%S")
        print("--------------------")
        print("Current Time =", self.currentTime)
        print(f"Looking in the land of {self.page['pluginDto']['name']} for {self.page['name']}")
        self.locationsList = self._coordinator.getScrapedPluginPages(self.page['id'],
                                                                     self.schedulerConf[0],
                                                                     self.autoPublish,
                                                                     self.templateName)
        print(f"Master, I found {len(self.locationsList)} new souls.")
        #Have to go with try, because many pages don't show percentage reduction
        try:
            self.locationsList = sorted(self.locationsList, key=['percentage'])
        except:
            pass
        self._view.addLocationsToList(self.locationsList)
        if(self.schedulerConf[0]):
            self._status.newScheduler(self._view.ui.gridLayout, self.secondaryList, self._nextRuns())
        self._view.show()
    #endregion

    #region Events from Below
    def deleteScheduler(self):
        self.scheduler.stopScheduler()

    def publishLocation(self, indexes):
        for index in indexes:
            self._coordinator.publishPageLocation(self.locationsList[index.row()])
        self._view.checkPublished()
    #endregion
