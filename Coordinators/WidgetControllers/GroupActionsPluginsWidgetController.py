# ActionsPluginsWidgetController

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Views.Widgets.GroupActionsPluginsWidgetUi import Ui_GroupActionsPluginsWidget


class GroupActionsPluginsWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupActionsPluginsWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupActionsPluginsWidget()