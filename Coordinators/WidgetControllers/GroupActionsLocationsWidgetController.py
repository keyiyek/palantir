# ActionsLocationsWidgetController

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Views.Widgets.GroupActionsLocationsWidgetUi import Ui_GroupActionsLocationsWidget


class GroupActionsLocationsWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupActionsLocationsWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupActionsLocationsWidget()