# ItemWidgetController

from PyQt5.QtWidgets import QWidget, QListWidgetItem

from Views.Widgets.ListItemSchedulerUi import Ui_ListItemScheduler


class ListItemSchedulerController:

    def __init__(self, coordinator):
        self.qWidgets = {}
        self._coordinator = coordinator
        self.uiPage = Ui_ListItemScheduler()
        self.uiList = Ui_ListItemScheduler()


    def _connectSignals(self):
        self.uiPage.butDelete.clicked.connect(lambda: self.deleteScheduler())
        self.uiList.butDelete.clicked.connect(lambda: self.deleteScheduler())

    #Status is a list [run, next, delta]
    #[String, String, String]
    def populateWidget(self, status):
        self.uiPage.labelPage.setText(self._coordinator.page["name"])
        self.uiPage.labelPlugin.setText(self._coordinator.page["pluginDto"]["name"])
        self.uiPage.labelLastChecked.setText(f"Around {status[0]}")
        self.uiPage.labelNextRun.setText(status[1])
        self.uiPage.labelFrequency.setText(f"{status[2]} minutes")
        self.uiList.labelPage.setText(self._coordinator.page["name"])
        self.uiList.labelPlugin.setText(self._coordinator.page["pluginDto"]["name"])
        self.uiList.labelLastChecked.setText(f"Around {status[0]}")
        self.uiList.labelNextRun.setText(status[1])
        self.uiList.labelFrequency.setText(f"{status[2]} minutes")

    #Status is a list [run, next, delta]
    #[String, String, String]
    def newScheduler(self, layout, layoutSecondaryList, status):
        self.layoutSecondaryList = layoutSecondaryList
        self.deleteWidget()
        self.groupWidgetPage = QWidget()
        self.groupWidgetList = QWidget()
        self.uiPage.setupUi(self.groupWidgetPage)
        self.uiList.setupUi(self.groupWidgetList)
        self._connectSignals()

        layout.addWidget(self.groupWidgetPage, 1, 0, 1, 2)
        self.item = QListWidgetItem(layoutSecondaryList)
        self.item.setSizeHint(self.groupWidgetList.size())

        layoutSecondaryList.addItem(self.item)
        layoutSecondaryList.setItemWidget(self.item, self.groupWidgetList)
        self.qWidgets["group1"] = self.groupWidgetPage
        self.qWidgets["group2"] = self.groupWidgetList

        self.populateWidget(status)

    def deleteWidget(self):
        try:
            self.qWidgets["group1"].deleteLater()
            self.layoutSecondaryList.takeItem(self.layoutSecondaryList.row(self.item))
            self.qWidgets["group2"].deleteLater()
        except:
            pass

    def deleteScheduler(self):
        self._coordinator.deleteScheduler()
        self.deleteWidget()