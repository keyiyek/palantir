# PluginSmallWidgetController

from PyQt5.QtGui import QPixmap

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Services.Filer import Filer
from Views.Widgets.GroupPluginSmallWidgetUi import Ui_GroupPluginSmallWidget


class GroupPluginSmallWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupPluginSmallWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupPluginSmallWidget()
        self._filer = Filer()

    def populateWidget(self, item):

        pix = self._filer.fetchPixMap(item['thumbnail'], (64, 84))

        urlLink = "<a href=\"" + item['url'] + "\">" + item['name'] + "</a>"
        self.ui.labelPluginName.setText(urlLink)
        self.ui.labelPluginIcon.setPixmap(pix)
