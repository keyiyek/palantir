# ActionsGamesWidgetController

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Views.Widgets.GroupActionsGamesWidgetUi import Ui_GroupActionsGamesWidget


class GroupActionsGamesWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupActionsGamesWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupActionsGamesWidget()