# ActionsPagesWidgetController

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Views.Widgets.GroupActionsPagesWidgetUi import Ui_GroupActionsPagesWidget


class GroupActionsPagesWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupActionsPagesWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupActionsPagesWidget()

    #region INTERNAL SETUP
    def showWidget(self, layout, group):
        super().showWidget(layout, group)
        self._populateComboTemplate()

    def _populateComboTemplate(self):
        templates = self._coordinator.getTemplatesRelay()
        for template in templates:
            self.ui.comboTemplate.addItem(template)

    def _connectSignals(self):
        self.ui.butScrape.clicked.connect(lambda: self._coordinator.scrapeSignalRelay(
            self.ui.checkSchedule.isChecked(),
            self.ui.spinTimeValue.value(),
            self.ui.comboTimeUnit.currentIndex(),
            self.ui.checkAutopublish.isChecked(),
            self.ui.comboTemplate.currentText()
        ))

        self.ui.butSave.clicked.connect(lambda: self._coordinator.saveSchedulerConfigurationRelay(
            self.ui.spinTimeValue.value(),
            self.ui.comboTimeUnit.currentIndex(),
            self.ui.comboTemplate.currentText(),
            self.ui.checkAutopublish.isChecked(),
        ))

        self.ui.butRunConfigutarion.clicked.connect(lambda: self._coordinator.runConfigurationRelay())
    #endregion

