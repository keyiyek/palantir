# ActionsGamesWidgetController

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Views.Widgets.GroupActionsNichesWidgetUi import Ui_GroupActionNichesWidget


class GroupActionsNichesWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupActionsNichesWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupActionNichesWidget()

