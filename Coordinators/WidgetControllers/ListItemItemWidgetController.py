# ItemWidgetController

from PyQt5.QtWidgets import QWidget

from Views.Widgets.ListItemItemWidgetUi import Ui_ListItemItemWidget


class ListItemItemWidgetController:

    def __init__(self, coordinator):
        self._coordinator = coordinator
        self._view = Ui_ListItemItemWidget()

    def populateWidget(self):
        pass

    def showWidget(self, layout, group):
        self.groupWidget = QWidget()
        self._view.setupUi(self.groupWidget)

        layout.addWidget(self.groupWidget, group[0], group[1], group[2], group[3])

    def deleteWidget(self):
        self.groupWidget.deleteLater()

    def resetWidget(self):
        pass
