# FilterWidgetController

from PyQt5.QtWidgets import QWidget

from Views.Widgets.GroupFilterWidgetUi import Ui_GroupFilterWidget


class GroupFilterWidgetController:

    def __init__(self, coordinator):
        self.qWidgets = {}
        self._coordinator = coordinator
        self.ui = Ui_GroupFilterWidget()

    def showWidget(self, layout):
        self.deleteWidget()
        self.groupWidget = QWidget()
        self.ui.setupUi(self.groupWidget)

        layout.addWidget(self.groupWidget)
        self.qWidgets["group"] = self.groupWidget

    def deleteWidget(self):
        try:
            self.qWidgets["group"].deleteLater()
        except:
            pass

    def resetWidget(self):
        pass

    def populateWidget(self, tab):
        getattr(self, "_setUp" + tab)()

    def _setUpGames(self):
        self.ui.groupFilter.setVisible(False)

    def _setUpLocations(self):
        self.ui.comboFilter.addItem("All")
        self.ui.comboFilter.addItem("Pending")
        self.ui.comboFilter.addItem("Approved")
        self.ui.comboFilter.addItem("Rejected")
        self._connectSignals()

    def _setUpPlugins(self):
        self.ui.comboFilter.addItem("All")
        self.ui.comboFilter.addItem("Active")
        self.ui.comboFilter.addItem("Inactive")
        self._connectSignals()

    def _setUpPages(self):
        self.ui.comboFilter.addItem("All")
        self.ui.comboFilter.addItem("Active")
        self.ui.comboFilter.addItem("Inactive")
        self.ui.comboFilter.setCurrentIndex(1)
        self._connectSignals()

    def _setUpOffers(self):
        self.ui.comboFilter.addItem("All")
        self.ui.comboFilter.addItem("Published")
        self.ui.comboFilter.addItem("Passed")
        self.ui.comboFilter.addItem("Ok")
        self._connectSignals()

    def _setUpNiches(self):
        self.ui.groupFilter.setVisible(False)

    def _connectSignals(self):
        self.ui.comboFilter.currentTextChanged.connect(
            lambda: self._coordinator.filterList())
        self.ui.textFilter.returnPressed.connect(
            lambda: self._coordinator.filterList())

    def _setUpMarkets(self):
        print("setUpMarkets:")

