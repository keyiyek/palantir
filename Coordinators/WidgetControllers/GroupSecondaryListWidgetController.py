# GroupWidgetController

from PyQt5.QtWidgets import QWidget, QListWidgetItem

from Views.Widgets.GroupSecondaryListWidgetUi import Ui_GroupLocationsWidget


class GroupSecondaryListWidgetController:

    def __init__(self, coordinator):
        self.qWidgets = {}
        self._coordinator = coordinator
        self.ui = Ui_GroupLocationsWidget()

    def showWidget(self, layout, group):
        self.deleteWidget()
        self.groupWidget = QWidget()
        self.ui.setupUi(self.groupWidget)

        layout.addWidget(self.groupWidget, group[0], group[1], group[2], group[3])
        self.qWidgets["group"] = self.groupWidget

        self._connectSignals()

    def _connectSignals(self):
        pass

    def deleteWidget(self):
        try:
            self.qWidgets["group"].deleteLater()
        except:
            pass

    def resetWidget(self):
        pass

    def getSecondaryListWidget(self):
        return self.ui.listSecondary