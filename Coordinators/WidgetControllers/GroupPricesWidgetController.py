# PricesWidgetController

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Views.Widgets.GroupPricesWidgetUi import Ui_GroupPricesWidget


class GroupPricesWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupPricesWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupPricesWidget()

