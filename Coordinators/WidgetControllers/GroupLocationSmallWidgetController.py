# LocationSmallWidgetController
import urllib

from PyQt5.QtGui import QPixmap

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Views.Widgets.GroupLocationSmallWidgetUi import Ui_GroupLocationSmallWidget


class GroupLocationSmallWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupLocationSmallWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupLocationSmallWidget()

    def populateWidget(self, item):
        try:
            imageUrl = item['thumbnail']
            data = urllib.request.urlopen(imageUrl).read()
            pix = QPixmap()
            pix.loadFromData(data)
            pix = pix.scaled(64, 84)
        except:
            imageUrl = "Resources/Images/baseIcon.png"
            pix = QPixmap(imageUrl).scaled(64, 84)

        urlLink = "<a href=\"" + item['url'] + "\">Vai alla Pagina</a>"
        self.ui.labelLocationTitle.setText(item['name'])
        self.ui.labelLocationLink.setText(urlLink)
        self.ui.labelLocationIcon.setPixmap(pix)