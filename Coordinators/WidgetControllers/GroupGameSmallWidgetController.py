# GameSmallWidgetController
import urllib

from PyQt5.QtGui import QPixmap

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Views.Widgets.GroupGameSmallWidgetUi import Ui_GroupGameSmallWidget


class GroupGameSmallWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupGameSmallWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupGameSmallWidget()

    def populateWidget(self, item):
        try:
            imageUrl = item['thumbnail']
            data = urllib.request.urlopen(imageUrl).read()
            pix = QPixmap()
            pix.loadFromData(data)
            pix = pix.scaled(64, 84)
        except:
            imageUrl = "Resources/Images/baseIcon.png"
            pix = QPixmap(imageUrl).scaled(64, 84)

        self.ui.labelGameTitle.setText(item['name'])
        self.ui.labelBggRank.setText(str(item['bggRank']))
        self.ui.labelBggScore.setText(str(item['bggScore']))
        self.ui.labelGameIcon.setPixmap(pix)