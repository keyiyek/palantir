# ActionsPricesWidgetController

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Views.Widgets.GroupActionsPricesWidgetUi import Ui_GroupActionsPricesWidget


class GroupActionsPricesWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupActionsPricesWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupActionsPricesWidget()