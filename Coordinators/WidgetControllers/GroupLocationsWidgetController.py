# LocationsWidgetController

from Coordinators.WidgetControllers.GroupWidgetController import GroupWidgetController
from Views.Widgets.GroupLocationsWidgetUi import Ui_GroupLocationsWidget


class GroupLocationsWidgetController(GroupWidgetController):

    def __init__(self, coordinator):
        super(GroupLocationsWidgetController, self).__init__(coordinator)
        self.ui = Ui_GroupLocationsWidget()
