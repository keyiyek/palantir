# SchedulerController
# to time the scrapes
import datetime

from PyQt5.QtCore import pyqtSignal, QObject
from apscheduler.schedulers.qt import QtScheduler
from apscheduler.triggers.interval import IntervalTrigger


class SchedulerController(QObject):
    populateSignal = pyqtSignal()

    def __init__(self, controller, timeValue, timeUnit):
        super().__init__()
        self._controller = controller
        # This is almost a joke
        # TimeUnit = 0 -> minutes
        # TimeUnit = 1 -> hours
        # 60^0 = 1, 60^1 = 60
        self.timeValue = timeValue * pow(60, timeUnit)

        self.scheduler = QtScheduler(job_defaults={'misfire_grace_time': self.timeValue * 30})
        self.scheduler.start(paused=False)
        self.populateSignal.connect(lambda: self._controller.scrape())

        self.scheduler.add_job(
            func=lambda: self.populateSignal.emit(),
            trigger=IntervalTrigger(minutes=self.timeValue,
                                    start_date=datetime.datetime.now() + datetime.timedelta(seconds=5)),
            id=str(self._controller.page['id']),
            name=self._controller.page['name'],
            replace_existing=True)

    def stopScheduler(self):
        print("Killing the messenger...")
        try:
            self.scheduler.shutdown()
        except:
            pass

    def getConfiguration(self):
        print(self.scheduler.get_job(self._controller.page['id']).trigger)
        print(self.scheduler.get_job(self._controller.page['id']).next_run_time)
