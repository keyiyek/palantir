#MainCoordinator
#This is is the central nevralgic core for Palantir
#All data passes through here, and all the Controllers are handled from here
from threading import Thread

from PyQt5.QtCore import QThread

from Coordinators.ViewMainController import ViewMainController
from Coordinators.ViewPageLocationsController import ViewPageLocationsController
from Services.Filer import Filer
from Services.SauronBridge import SauronBridge

class MainCoordinator:

    def __init__(self):
        print("Stare into me. I'll give you knowledge.")
        self._bridge = SauronBridge()
        self._filer = Filer()
        self.ViewPageLocationControllers = {}

        self.createMainController()


    #region Sauron Calls
    def askSauron(self, path):
        return self._bridge.getModel(path)

    def getScrapedPluginPages(self, pluginPageId, scheduled, autoPublish, templateName):

        return self._bridge.scrapePages(pluginPageId, scheduled, autoPublish, templateName)

    def publishPageLocation(self, location):
        self._bridge.publishPageLocation(location)
    #endregion

    #region Filer Calls
    def saveDict(self, dicT, pathName):
        self._filer.saveFile(pathName, dicT)

    def getDict(self, pathName):
        return self._filer.fetchDict(pathName)

    def getPath(self, name):
        return self._filer.paths[name]

    def getFile(self, pathName):
        return self._filer.fetchFile(pathName)
    #endregion

    #region Main
    def runConfiguredSchedulers(self, schedulers):
        for item in schedulers:
            scheduler = [True, schedulers[item]["timeValue"], schedulers[item]["timeUnit"]]
            autopublish = schedulers[item]["autopublish"]
            template = schedulers[item]["templateName"]

            self.createPageLocationController(schedulers[item]["page"], scheduler, autopublish, template)

    def createMainController(self):
        self._mainController = ViewMainController(self)

    def createPageLocationController(self, item, scheduler, autoPublish, templateName):
        self.viewPageLocationsController = ViewPageLocationsController(self, item, scheduler, autoPublish, templateName, self._mainController.getSecondaryListWidget())
        self.viewPageLocationsController.startScheduler()
        self.ViewPageLocationControllers[item['name']+":"+str(item['id'])] = self.viewPageLocationsController
    #endregion

    #region Settings
    def saveSchedulerConfiguration(self, item, timeValue, timeUnit, templateName, autopublish):
        allConfigurations = self.getDict("SchedulerConfigurations")
        allConfigurations[item['name']+":"+str(item['id'])] = {"page": item,
                                           "timeValue": timeValue,
                                           "timeUnit": timeUnit,
                                           "templateName": templateName,
                                           "autopublish": autopublish}
        self._filer.saveFile("SchedulerConfigurations", allConfigurations)

    def saveServerIp(self, ip):
        self._filer.saveFile("ServerIp", ip)
        self._bridge.updateServerIp()
    #endregion




