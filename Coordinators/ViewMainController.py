# MainViewController
# From here we take care of all the MainView processes
from Coordinators.ViewPageLocationsController import ViewPageLocationsController
from Coordinators.SplitLeftViewController import SplitLeftViewController
from Coordinators.SplitRightViewController import SplitRightViewController
from Coordinators.WidgetControllers.ListItemSchedulerController import ListItemSchedulerController
from Services.Filer import Filer
from Views.ViewMain import ViewMain
from Views.ViewSettings import ViewSettings


class ViewMainController:

    def __init__(self, coordinator):
        print("Here, I'll show you far away")
        self._coordinator = coordinator
        self.splitRightLayout = "GroupsLayout"
        self.filer = Filer()
        self.primaryList = []
        self.primaryListSelectedIndexes = []
        self.primaryVisible = []
        self.groups = {}
        self.templates = []
        self.ViewPageLocationControllers = {}

        self._view = ViewMain(self)
        self._splitLeftController = SplitLeftViewController(self)
        self._splitRightController = SplitRightViewController(self)

        self._loadView()

    #region INTERNAL SETUP
    def _loadView(self):
        self.groups = self._coordinator.getDict(self.splitRightLayout)
        self.templates = self._coordinator.askSauron("Templates")
        self._view.setComboTabs(self.groups)
        self._view.show()

    #Not Sure What This Does
    #def getPath(self, name):
        #return self._coordinator.getPath(name)
    #endregion

    def getTemplates(self):
        return self.templates

    def getSchedulers(self):
        return self._coordinator.getDict("SchedulerConfigurations")

    #region INCOMING
    #region Events from VIEWMAIN
    def tabChanged(self, text):
        print("Perfect choice, Master")
        self._view.comboText = text
        self._splitRightController.showGroups(self._view.ui.gridLayout_2, self.groups[text])
        self._splitLeftController.tabChanged(self._view.ui.leftLayout, text)
        try:
            self.primaryList = self._coordinator.askSauron(text)
        except:
            pass
        self._splitLeftController.filterList()

    def selectionListPrimaryChanged(self, indexes):
        self.primaryListSelectedIndexes = indexes
        if (len(self.primaryListSelectedIndexes) == 1):
            self._splitRightController.populateGroups(self._view.comboText,
                                                      self.primaryVisible[self.primaryListSelectedIndexes[0].row()])
        # else:
        # self.populator.clearGroups()

    def showSettings(self):
        self.setting = ViewSettings(self)
        self.setting.show()

    #endregion

    #region Events from LEFTSPLIT
    def filterList(self, text, combo):
        if combo == "All" and text == '':
            self.primaryVisible = self.primaryList
        if combo != "All" and text == '':
            self.primaryVisible = list(filter(lambda item: item['status'] == combo.upper(), self.primaryList))
        if combo == "All" and text != '':
            self.primaryVisible = list(filter(lambda item: item['name'] == text, self.primaryList))
        if combo != "All" and text != '':
            self.primaryVisible = list(
                filter(lambda item: item['name'] == text and item['status'] == combo.upper(), self.primaryList))

        self.sendListToShow()
    #endregion

    #region Events from RIGHTSPLIT
    #scheduler is an array [bool, int, int]
    #[scheduled, timeValue, timeUnit]
    def scrape(self, scheduler, autoPublish, templateName):
        for index in self.primaryListSelectedIndexes:
            item = self.primaryVisible[index.row()]
            self._coordinator.createPageLocationController(item, scheduler, autoPublish, templateName)

    def saveSchedulerConfiguration(self, timeValue, timeUnit, templateName, autopublish):
        for index in self.primaryListSelectedIndexes:
            item = self.primaryVisible[index.row()]
            item['pluginDto']['gameLocations'] = None
            item['pluginDto']['pluginPages'] = None
            item['pageLocations'] = None
            self._coordinator.saveSchedulerConfiguration(item, timeValue, timeUnit, templateName, autopublish)

    def runConfigurations(self):
        configuration = self.getSchedulers()
        self._coordinator.runConfiguredSchedulers(configuration)
        #self._coordinator.createPageLocationController(configuration[item]["page"],
        #                                                   [True, configuration[item]["timeValue"], configuration[item]["timeUnit"]],
        #                                                   configuration[item]["autopublish"],
        #                                                   configuration[item]["templateName"])
    #endregion

    #region Events fom SETTINGS
    def saveServerIp(self, ip):
        self._coordinator.saveServerIp(ip)
        self.setting.close()

    def getSchedulers(self):
        return self._coordinator.getDict("SchedulerConfigurations")

    def saveScheduler(self, schedulers):
        self._coordinator.saveDict(schedulers, "SchedulerConfigurations")

    def getServerIP(self):
        return self._coordinator.getFile("ServerIp")
    #endregion

    #endregion

    #region OUTGOING
    #region Data ti MAINVIEW
    def sendListToShow(self):
        try:
            self.primaryVisible = sorted(self.primaryVisible, key=lambda i: i['plugin']['name'])
        except:
            pass
        self._view.addItemsToList(self.primaryVisible, self._view.ui.listPrimary)

    def getSecondaryListWidget(self):
        return self._splitRightController.getSecondaryListWidget()
    #endregion
    #endregion