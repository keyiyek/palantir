# SplitLeftViewController
# Handles the data of everything that happens in the right side of the slitter
from Coordinators.WidgetControllers.GroupFilterWidgetController import GroupFilterWidgetController
from Coordinators.SplitViewController import SplitViewController


class SplitLeftViewController(SplitViewController):

    def __init__(self, coordinator):
        super(SplitLeftViewController, self).__init__(coordinator)
        print("Oh, we are going to do great things together!")
        self._filter = GroupFilterWidgetController(self)

    def tabChanged(self, layout, tabText):
        self._filter.deleteWidget()
        self._filter.showWidget(layout)
        self._filter.populateWidget(tabText)

    def filterList(self):
        name = self._filter.ui.textFilter.text()
        combo = self._filter.ui.comboFilter.currentText()
        self._coordinator.filterList(name, combo)