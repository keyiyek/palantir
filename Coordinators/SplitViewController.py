
class SplitViewController:

    def __init__(self, coordinator):
        self._coordinator = coordinator
        self.groupsList = {}

    def deleteGroups(self):
        for group in self.groupsList:
            self.groupsList[group][0].deleteLater()
        self.groupsList = {}