# SplitRightViewController
# Handles the data of everythong that happens in the right side of the slitter
import importlib

from Coordinators.SplitViewController import SplitViewController


class SplitRightViewController(SplitViewController):

    def __init__(self, coordinator):
        print("Yes... Yes... closer... a little closer...")
        self._coordinator = coordinator
        self.groupsList = {}
        self.locations = []

    #region INCOMING DATA
    def showGroups(self, layout, groups):
        # groups is a dictionary with widgetName: [layoutCoordinates]

        for group in self.groupsList:
            self.groupsList[group].deleteWidget()

        for group in groups:
            if group in self.groupsList:
                self.groupsList[group].showWidget(layout, groups[group])
            else:
                cModule = importlib.import_module(f"Coordinators.WidgetControllers.{group}Controller")
                groupController = getattr(cModule, f"{group}Controller")(self)
                groupController.showWidget(layout, groups[group])
                self.groupsList[group] = groupController

    def populateGroups(self, type, item):
        getattr(self, "_populate" + type)(item)
    #endregion

    #region OUTGOING SIGNALS
    #region UPWARDS
    def scrapeSignalRelay(self, scheduled, timeValue, timeUnit, autoPublish, templateName):
        scheduler = [scheduled, timeValue, timeUnit]
        self._coordinator.scrape(scheduler, autoPublish, templateName)

    def saveSchedulerConfigurationRelay(self, timeValue, timeUnit, templateName, autopublish):
        self._coordinator.saveSchedulerConfiguration(timeValue, timeUnit, templateName, autopublish)

    def runConfigurationRelay(self):
        self._coordinator.runConfigurations()

    def getTemplatesRelay(self):
        return self._coordinator.getTemplates()
    #endregion

    #region DOWNWARDS
    def clearGroups(self):
        pass

    #region Specific Groups
    def _populateGames(self, item, first=True):
        self.groupsList["GroupGameSmallWidget"].populateWidget(item)
        """
        if (first):
            listSecondary = self.groupsList['LocationsWidget'][1].listSecondary
            self.locations = []
            self.locations = item['gameLocations']
            self._coordinator._view.addItemsToList(item['ameLocations'], listSecondary, Ui_ItemWidget())
            listSecondary.itemSelectionChanged.connect(lambda: self.selectionListSecondaryChanged())
        """

    def _populateLocations(self, item, first=True):
        self.groupsList["GroupLocationSmallWidget"].populateWidget(item)
        self._populatePlugins(item['plugin'], False)

        if first:
            self._populateGames(item['gameTotal'], False)

    def _populatePlugins(self, item, first=True):
        self.groupsList["GroupPluginSmallWidget"].populateWidget(item)

    def _populatePrices(self, item, first=True):
        print("populateGameSmall:")

    def _populateActions(self, item, first=True):
        print("populateGameSmall:")

    def _populatePages(self, item, first=True):
        self._populatePlugins(item['pluginDto'], False)

    def _populateSchedulers(self, schedulers):
        print("poplateScheduler:")
        #self._coordinator._view.addSimpleItemsToList(schedulers, self.groupsList['LocationsWidget'][1].listSecondary)

    def getSecondaryListWidget(self):
        return self.groupsList['GroupSecondaryListWidget'].getSecondaryListWidget()
    #endregion
    #endregion
    #endregion