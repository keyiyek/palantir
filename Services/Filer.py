# Filer
# From here we grab data from local files
import ast
import urllib

from PyQt5.QtGui import QPixmap


class Filer():

    def __init__(self):
        self.paths = {}

        self._setPaths()

    def _setPaths(self):
        self.paths["paths"] = "Resources/Configurations/paths.txt"
        self.paths = self.fetchDict("paths")

    # region Private Methods
    def _litteralEval(self, string):
        return ast.literal_eval(string)

    # def _setDictTwo(self, file):
    #     print("setDictTwo")
    #     dicT = {}
    #     lisT = file.readlines()
    #
    #     for item in lisT:
    #         (key, val) = item.split()
    #         dicT[key] = val
    #     return dicT
    #
    # def _setDictThree(self, file):
    #     print("setDictThree")
    #     dicT = {}
    #     lisT = file.readlines()
    #
    #     for item in lisT:
    #         (key, val, cat) = item.split()
    #         dicT[key] = (val, cat)
    #     return dicT

    # endregion

    # def fetchDictThree(self, path):
    #     print("fetchDictThree:")
    #     with open(path) as infile:
    #         dicT = self._setDictThree(infile)
    #     return dicT
    #
    # def fetchDictTwo(self, path):
    #     print("fetchDictTwo:")
    #     with open(path) as infile:
    #         dicT = self._setDictTwo(infile)
    #     return dicT

    def fetchDict(self, pathName):
        try:
            with open(self.paths[pathName], "r") as infile:
                text = infile.read()
            dicT = self._litteralEval(text)
            return dicT
        except:
            pass

    def fetchLines(self, path):
        print("fetchLines:")
        with open(path) as infile:
            lisT = infile.readlines()
        return lisT

    def fetchPixMap(self, url, size):
        try:
            data = urllib.request.urlopen(url).read()
            pix = QPixmap()
            pix.loadFromData(data)
            pix = pix.scaled(size[0], size[1])
        except:
            imageUrl = self.paths['BaseIcon']
            pix = QPixmap(imageUrl).scaled(size[0], size[1])

        return pix

    def fetchLocalPixMap(self, path, size):
        try:
            imageUrl = self.paths[path]
            pix = QPixmap(imageUrl).scaled(size[0], size[1])
        except:
            imageUrl = self.paths['BaseIcon']
            pix = QPixmap(imageUrl).scaled(size[0], size[1])

        return pix

    def saveFile(self, pathName, content):
        with open(self.paths[pathName], "w") as outfile:
            outfile.write(str(content))

    def fetchFile(self, pathName):
        try:
            with open(self.paths[pathName], "r") as infile:
                text = infile.read()
            return text
        except:
            pass
