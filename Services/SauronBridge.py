#SauronBridge
#This handles the rest calls to Sauron

import json
import requests
from string import Template

from Services.Filer import Filer


class SauronBridge():

    def __init__(self):
        print("Calling The One That Should Be Worshipped")
        self.fetcher = Filer()
        self.entryPoints = self.fetcher.fetchDict("SauronEntryPoints")
        self.serverIp = ""
        self.updateServerIp()

    def getModel(self, path):
        parsedPath = Template(self.entryPoints[path])
        parsedPath = parsedPath.substitute(serverIp=self.serverIp)
        pageList = []
        try:
            response = requests.get(parsedPath)
            pageList = json.loads(response.text)
        except:
            pass
        return pageList

    def scrapePages(self, pageId, scheduled, autoPublish, templateName):
        print("The One-Eye is lurking...")
        path = self.entryPoints["scrapePluginPageId"]
        templatePath = Template(path)
        parsedPath = templatePath.substitute(serverIp=self.serverIp, id=pageId, tName=templateName, scheduled=scheduled, publisher=autoPublish)
        locationList = []
        try:
            response = requests.get(parsedPath)
            locationList = json.loads(response.text)
        except:
            pass
        return locationList

    def publishPageLocation(self, location):
        print("Let your subjects know your will.")
        templatePath = Template(self.entryPoints["publishPageLocation"])
        parsedPath = templatePath.substitute(serverIp=self.serverIp, id=location["id"])
        try:
            requests.get(parsedPath)
        except:
            pass


    def updateServerIp(self):
        self.serverIp = self.fetcher.fetchFile("ServerIp")
