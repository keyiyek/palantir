#ViewSettings
from PyQt5.QtWidgets import QWidget, QTableWidgetItem

from Views.Widgets.ViewSettingsUi import Ui_ViewSettings


class ViewSettings(QWidget):

    def __init__(self, controller):
        super(ViewSettings, self).__init__()
        self._controller = controller
        self.ui = Ui_ViewSettings()
        self.schedulers = {}

        self.ui.setupUi(self)
        self.displaySchedulers()
        self.displayServerIP()
        self._connectSignals()

    def _connectSignals(self):
        self.ui.butServerIp.clicked.connect(lambda: self._controller.saveServerIp(self.ui.textServerIp.text()))
        self.ui.butDelete.clicked.connect(lambda: self.deleteScheduler())

    def displayServerIP(self):
        self.ui.textServerIp.setText(self._controller.getServerIP())

    def displaySchedulers(self):
        try:
            self.ui.tableScheduler.cellChanged.disconnect()
        except:
            pass
        self.ui.tableScheduler.clear()
        self.schedulers = self._controller.getSchedulers()
        row = 0
        self.ui.tableScheduler.setRowCount(len(self.schedulers))
        for scheduler in self.schedulers:
            self.ui.tableScheduler.setItem(row, 0, QTableWidgetItem(scheduler))
            self.ui.tableScheduler.setItem(row, 1, QTableWidgetItem(self.schedulers[scheduler]["page"]["pluginDto"]["name"]))
            self.ui.tableScheduler.setItem(row, 2, QTableWidgetItem(str(self.schedulers[scheduler]["timeValue"])))
            self.ui.tableScheduler.setItem(row, 3, QTableWidgetItem("minutes" if self.schedulers[scheduler]["timeUnit"] == 0 else "hours"))

            row = row + 1

        self.ui.tableScheduler.cellChanged.connect(lambda: self.saveItem())

    def deleteScheduler(self):
        name = self.ui.tableScheduler.currentItem().text()
        self.schedulers.pop(name)
        self._controller.saveScheduler(self.schedulers)
        self.displaySchedulers()

    def saveItem(self):
        selectedRow = self.ui.tableScheduler.currentIndex().row()
        itemName = self.ui.tableScheduler.item(selectedRow, 0).text()
        timeValue = self.ui.tableScheduler.item(selectedRow, 2).text()
        timeUnit = self.ui.tableScheduler.item(selectedRow, 3).text()
        timeUnit = 0 if timeUnit == "minutes" else 1
        timeValue = int(timeValue)
        self.schedulers[itemName]["timeValue"] = timeValue
        self.schedulers[itemName]["timeUnit"] = timeUnit
        self._controller.saveScheduler(self.schedulers)