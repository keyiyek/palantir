#MainView
#The displayer of the data
from PyQt5.QtWidgets import QMainWindow, QWidget, QListWidgetItem

from Services.Filer import Filer
from Views.Widgets.ListItemItemWidgetUi import Ui_ListItemItemWidget
from Views.Widgets.ListItemPageWidgetUi import Ui_ListItemPageWidget
from Views.Widgets.ViewMainUi import Ui_ViewMain


class ViewMain(QMainWindow):

    def __init__(self, controller):
        super(ViewMain, self).__init__()
        print("This knowledge will make you powerful")
        self._controller = controller
        self.setWindowTitle("Palantir")
        self.ui = Ui_ViewMain()
        self._filer = Filer()

        self._initUI()
        self._connectSignals()

        self.confs = {}
        self.schedulers = {}
        self.comboTabs = {}
        self.widgetControllers = []
        self.comboText = []

    #region INTERNAL SETUP
    def _initUI(self):
        self.ui.setupUi(self)

    def _connectSignals(self):
        self.ui.comboTabs.currentTextChanged.connect(lambda: self._controller.tabChanged(self.ui.comboTabs.currentText()))
        self.ui.listPrimary.itemSelectionChanged.connect(lambda: self._controller.selectionListPrimaryChanged(self.ui.listPrimary.selectedIndexes()))
        self.ui.actionSettings.triggered.connect(lambda: self._controller.showSettings())
    #endregion

    #region INCOMING DATA
    def setComboTabs(self, dicT):
        self.confs = dicT
        self.ui.comboTabs.addItems(self.confs.keys())

    #region AddItemsToList
    def addItemsToList(self, items, widget):
        getattr(self, "_addItemsToList" + self.comboText)(items, widget)

    def _addItemsToListGames(self, games, widget):
        self._addItemsToList(games, widget, Ui_ListItemItemWidget())

    def _addItemsToListLocations(self, locations, widget):
        self._addItemsToList(locations, widget, Ui_ListItemItemWidget())

    def _addItemsToListPlugins(self, plugins, widget):
        self._addItemsToList(plugins, widget, Ui_ListItemItemWidget())

    def _addItemsToListPages(self, pages, widget):
        self.schedulers = self._controller.getSchedulers()
        self._addItemsToList(pages, widget, Ui_ListItemPageWidget())

    def _addItemsToListOffers(self, offers, widget):
        self._addItemsToList(offers, widget, Ui_ListItemItemWidget())

    def _addItemsToListNiches(self, niches, widget):
        self._addItemsToList(niches, widget, Ui_ListItemItemWidget())

    def _addItemsToList(self, items, lisT, widget):
        lisT.clear()
        for item in items:
            rowWidget = QWidget()
            widget.setupUi(rowWidget)
            rowWidget.show()
            try:
                name = item['name']
            except:
                name = "--"
            try:
                status = item['status']
            except:
                status = "--"

            pix = self._filer.fetchPixMap(item['pluginDto']['thumbnail'], (64, 84))

            widget.labelTitle.setText(name)
            widget.labelStatus.setText(status)
            try:
                widget.labelThumbnail.setPixmap(pix)
            except:
                pass
            timeUnit = ""
            timeValue = ""
            try:
                timeValue = str(self.schedulers[item["name"]+":"+str(item["id"])]["timeValue"])
                timeUnit = "minutes" if self.schedulers[item["name"]+":"+str(item["id"])]["timeUnit"] == 0 else "hours"
            except:
                timeValue = "--"
                timeUnit = "--"

            widget.labelTimeValue.setText(timeValue)
            widget.labelTimeUnit.setText(timeUnit)

            item = QListWidgetItem(lisT)
            item.setSizeHint(rowWidget.size())

            lisT.addItem(item)
            lisT.setItemWidget(item, rowWidget)
    #endregion

