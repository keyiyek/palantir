# ViewPageLocations
# the view to list all pagelocation scraped

from PyQt5.QtWidgets import QWidget, QListWidgetItem

from Coordinators.WidgetControllers.ListItemSchedulerController import ListItemSchedulerController
from Services.Filer import Filer
from Views.ListItemLocationWidget import ListItemLocationWidget
from Views.Widgets.ListItemLocationWidgetUi import Ui_ListItemLocationWidget
from Views.Widgets.ViewPageLocationsUi import Ui_ViewPageLocations

class ViewPageLocations(QWidget):

    def __init__(self, controller):
        super(ViewPageLocations, self).__init__()
        print("My master..., I bring something to your attention...")
        self._controller = controller
        self.ui = Ui_ViewPageLocations()
        self._filer = Filer()
        self.widgetControllerList = []

        self.page = self._controller.page

        self._initUI()
        self._connectSignals()

    def _initUI(self):
        self.ui.setupUi(self)
        self.ui.labelPage.setText(self.page['name'])
        self.ui.labelPlugin.setText(self.page['pluginDto']['name'])
        self.ui.labelIcon.setPixmap(self._filer.fetchPixMap(self.page['pluginDto']['thumbnail'], (300, 40)))


    def _connectSignals(self):
        self.ui.butPublish.clicked.connect(lambda: self._controller.publishLocation(self.ui.listPageLocations.selectedIndexes()))

    def checkPublished(self):
        for index in self.ui.listPageLocations.selectedIndexes():
            pixPublished = self._filer.fetchLocalPixMap("CheckIcon", (91,61))
            self.widgetControllerList[index.row()].setLabelPublishedIcon(pixPublished)

    def addLocationsToList(self, items):
        self._addItemsToList(items)

    def _addItemsToList(self, items):
        self.widgetControllerList.clear()
        self.ui.listPageLocations.clear()
        for location in items:
            widgetController = ListItemLocationWidget(self)
            self.widgetControllerList.append(widgetController)
            widget = widgetController.getUi()
            rowWidget = QWidget()
            widget.setupUi(rowWidget)
            rowWidget.show()
            try:
                locationId = location['id']
            except:
                locationId = "--"
            try:
                title = location['name']
            except:
                title = "--"
            try:
                price = location['price']
            except:
                price = "--"
            try:
                oldPrice = location['oldPrice']
            except:
                oldPrice = "--"
            try:
                percentage = location['percentage']
            except:
                percentage = "--"
            try:
                reduction = location['reduction']
            except:
                reduction = "--"
            try:
                urlLink = "<a href=\"" + location['url'] + "\">link</a>"
            except:
                urlLink = "Link"
            thumbnailUrl = ""
            try:
                thumbnailUrl = location['thumbnail']
            except:
                thumbnailUrl = "--"
            try:
                availability = location['availability']
            except:
                availability = "--"
            pixThumbnail = self._filer.fetchPixMap(thumbnailUrl, (91, 61))
            pixPublished = self._filer.fetchLocalPixMap("BlankIcon", (91, 61))

            widget.labelPublished.setPixmap(pixPublished)
            widget.labelTitle.setText(title)
            widget.labelPrice.setText(str(price))
            widget.labelOldPrice.setText(str(oldPrice))
            widget.labelSaving.setText(str(reduction))
            widget.labelSconto.setText(str(percentage))
            widget.labelAvailability.setText(availability)
            widget.labelIcon.setPixmap(pixThumbnail)
            widget.labelUrl.setText(urlLink)

            # print("adding row to list")
            item = QListWidgetItem(self.ui.listPageLocations)
            item.setSizeHint(rowWidget.size())

            self.ui.listPageLocations.addItem(item)
            self.ui.listPageLocations.setItemWidget(item, rowWidget)