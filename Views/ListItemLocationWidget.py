# ViewSettings
from PyQt5.QtWidgets import QWidget

from Views.Widgets.ListItemLocationWidgetUi import Ui_ListItemLocationWidget
from Views.Widgets.ViewSettingsUi import Ui_ViewSettings


class ListItemLocationWidget(QWidget):

    def __init__(self, controller):
        super(ListItemLocationWidget, self).__init__()
        self._controller = controller
        self.ui = Ui_ListItemLocationWidget()

        self.ui.setupUi(self)

    def setLabelPublishedIcon(self, pix):
        self.ui.labelPublished.setPixmap(pix)

    def getUi(self):
        return self.ui
