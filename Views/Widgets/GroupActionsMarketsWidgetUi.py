# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/Users/hrs/Projects/code/Palantir/Views/Uis/GroupActionsMarketsWidgetUi.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_GroupActionMarketsWidget(object):
    def setupUi(self, GroupActionMarketsWidget):
        GroupActionMarketsWidget.setObjectName("GroupActionMarketsWidget")
        GroupActionMarketsWidget.resize(364, 198)
        self.gridLayout = QtWidgets.QGridLayout(GroupActionMarketsWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.groupActions = QtWidgets.QGroupBox(GroupActionMarketsWidget)
        self.groupActions.setObjectName("groupActions")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.groupActions)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.butTop = QtWidgets.QPushButton(self.groupActions)
        self.butTop.setObjectName("butTop")
        self.gridLayout_7.addWidget(self.butTop, 0, 0, 1, 1)
        self.butLeft = QtWidgets.QPushButton(self.groupActions)
        self.butLeft.setObjectName("butLeft")
        self.gridLayout_7.addWidget(self.butLeft, 2, 1, 1, 1)
        self.butRight = QtWidgets.QPushButton(self.groupActions)
        self.butRight.setObjectName("butRight")
        self.gridLayout_7.addWidget(self.butRight, 2, 3, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_7.addItem(spacerItem, 1, 1, 1, 1)
        self.gridLayout.addWidget(self.groupActions, 0, 0, 1, 1)

        self.retranslateUi(GroupActionMarketsWidget)
        QtCore.QMetaObject.connectSlotsByName(GroupActionMarketsWidget)

    def retranslateUi(self, GroupActionMarketsWidget):
        _translate = QtCore.QCoreApplication.translate
        GroupActionMarketsWidget.setWindowTitle(_translate("GroupActionMarketsWidget", "Form"))
        self.groupActions.setTitle(_translate("GroupActionMarketsWidget", "Actions"))
        self.butTop.setText(_translate("GroupActionMarketsWidget", "PushButton"))
        self.butLeft.setText(_translate("GroupActionMarketsWidget", "Check"))
        self.butRight.setText(_translate("GroupActionMarketsWidget", "Publish"))
