# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Views/Uis/ListItemSchedulerUi.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ListItemScheduler(object):
    def setupUi(self, ListItemScheduler):
        ListItemScheduler.setObjectName("ListItemScheduler")
        ListItemScheduler.resize(748, 90)
        self.gridLayout = QtWidgets.QGridLayout(ListItemScheduler)
        self.gridLayout.setObjectName("gridLayout")
        self.label_2 = QtWidgets.QLabel(ListItemScheduler)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 2, 1, 1)
        self.labelPage = QtWidgets.QLabel(ListItemScheduler)
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.labelPage.setFont(font)
        self.labelPage.setObjectName("labelPage")
        self.gridLayout.addWidget(self.labelPage, 0, 0, 1, 1)
        self.labelLastChecked = QtWidgets.QLabel(ListItemScheduler)
        self.labelLastChecked.setObjectName("labelLastChecked")
        self.gridLayout.addWidget(self.labelLastChecked, 0, 4, 1, 1)
        self.label_1 = QtWidgets.QLabel(ListItemScheduler)
        self.label_1.setObjectName("label_1")
        self.gridLayout.addWidget(self.label_1, 0, 3, 1, 1)
        self.labelNextRun = QtWidgets.QLabel(ListItemScheduler)
        self.labelNextRun.setObjectName("labelNextRun")
        self.gridLayout.addWidget(self.labelNextRun, 1, 1, 1, 1)
        self.labelPlugin = QtWidgets.QLabel(ListItemScheduler)
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.labelPlugin.setFont(font)
        self.labelPlugin.setObjectName("labelPlugin")
        self.gridLayout.addWidget(self.labelPlugin, 0, 2, 1, 1)
        self.label = QtWidgets.QLabel(ListItemScheduler)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.labelFrequency = QtWidgets.QLabel(ListItemScheduler)
        self.labelFrequency.setObjectName("labelFrequency")
        self.gridLayout.addWidget(self.labelFrequency, 1, 3, 1, 1)
        self.butDelete = QtWidgets.QPushButton(ListItemScheduler)
        self.butDelete.setObjectName("butDelete")
        self.gridLayout.addWidget(self.butDelete, 1, 4, 1, 1)

        self.retranslateUi(ListItemScheduler)
        QtCore.QMetaObject.connectSlotsByName(ListItemScheduler)

    def retranslateUi(self, ListItemScheduler):
        _translate = QtCore.QCoreApplication.translate
        ListItemScheduler.setWindowTitle(_translate("ListItemScheduler", "Form"))
        self.label_2.setText(_translate("ListItemScheduler", "Every:"))
        self.labelPage.setText(_translate("ListItemScheduler", "Page"))
        self.labelLastChecked.setText(_translate("ListItemScheduler", "LastChecked"))
        self.label_1.setText(_translate("ListItemScheduler", "Last Checked:"))
        self.labelNextRun.setText(_translate("ListItemScheduler", "Next Run"))
        self.labelPlugin.setText(_translate("ListItemScheduler", "Plugin"))
        self.label.setText(_translate("ListItemScheduler", "Scheduled at:"))
        self.labelFrequency.setText(_translate("ListItemScheduler", "Runs every"))
        self.butDelete.setText(_translate("ListItemScheduler", "Delete"))
