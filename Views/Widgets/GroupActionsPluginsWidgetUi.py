# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/Users/hrs/Projects/code/Palantir/Views/Uis/GroupActionsPluginsWidgetUi.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_GroupActionsPluginsWidget(object):
    def setupUi(self, GroupActionsPluginsWidget):
        GroupActionsPluginsWidget.setObjectName("GroupActionsPluginsWidget")
        GroupActionsPluginsWidget.resize(416, 198)
        self.gridLayout = QtWidgets.QGridLayout(GroupActionsPluginsWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.groupActions = QtWidgets.QGroupBox(GroupActionsPluginsWidget)
        self.groupActions.setObjectName("groupActions")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.groupActions)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.checkActivate = QtWidgets.QCheckBox(self.groupActions)
        self.checkActivate.setObjectName("checkActivate")
        self.gridLayout_7.addWidget(self.checkActivate, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.groupActions, 0, 0, 1, 1)

        self.retranslateUi(GroupActionsPluginsWidget)
        QtCore.QMetaObject.connectSlotsByName(GroupActionsPluginsWidget)

    def retranslateUi(self, GroupActionsPluginsWidget):
        _translate = QtCore.QCoreApplication.translate
        GroupActionsPluginsWidget.setWindowTitle(_translate("GroupActionsPluginsWidget", "Form"))
        self.groupActions.setTitle(_translate("GroupActionsPluginsWidget", "Actions"))
        self.checkActivate.setText(_translate("GroupActionsPluginsWidget", "Activate"))
