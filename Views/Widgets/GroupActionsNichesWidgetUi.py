# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Views/Uis/GroupActionNichesWidgetUi.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_GroupActionNichesWidget(object):
    def setupUi(self, GroupActionNichesWidget):
        GroupActionNichesWidget.setObjectName("GroupActionNichesWidget")
        GroupActionNichesWidget.resize(393, 264)
        self.gridLayout_2 = QtWidgets.QGridLayout(GroupActionNichesWidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.groupBox = QtWidgets.QGroupBox(GroupActionNichesWidget)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.textKeywords = QtWidgets.QLineEdit(self.groupBox)
        self.textKeywords.setObjectName("textKeywords")
        self.gridLayout.addWidget(self.textKeywords, 0, 0, 1, 1)
        self.butAdd = QtWidgets.QPushButton(self.groupBox)
        self.butAdd.setObjectName("butAdd")
        self.gridLayout.addWidget(self.butAdd, 0, 1, 1, 1)
        self.line = QtWidgets.QFrame(self.groupBox)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 1, 0, 1, 2)
        self.butSearch = QtWidgets.QPushButton(self.groupBox)
        self.butSearch.setObjectName("butSearch")
        self.gridLayout.addWidget(self.butSearch, 2, 1, 1, 1)
        self.gridLayout_2.addWidget(self.groupBox, 0, 0, 1, 1)

        self.retranslateUi(GroupActionNichesWidget)
        QtCore.QMetaObject.connectSlotsByName(GroupActionNichesWidget)

    def retranslateUi(self, GroupActionNichesWidget):
        _translate = QtCore.QCoreApplication.translate
        GroupActionNichesWidget.setWindowTitle(_translate("GroupActionNichesWidget", "Form"))
        self.groupBox.setTitle(_translate("GroupActionNichesWidget", "Actions"))
        self.butAdd.setText(_translate("GroupActionNichesWidget", "Add"))
        self.butSearch.setText(_translate("GroupActionNichesWidget", "Search"))
